module matrix_parser

import strconv
import math.fractions

// Function that takes a latex-formatted matrix string and returns
pub fn parse_cli_matrix(matrix string) [][]fractions.Fraction {
	/*
	Format of the matrix should be a string like:
	a&b\\c&d\\
	*/
	// Pull the rows out
	parsed_rows := matrix.split(r'\\')
	first_row := parsed_rows[0].split('&')
	// make a matrix of the correct size to store our values
	mut a := [][]fractions.Fraction{len: parsed_rows.len, init: []fractions.Fraction{len: first_row.len}}

	// iterate over all the rows and columns in string format, converting to float format
	for i, row in parsed_rows {
		parsed_elements := row.split('&')
		for j, element in parsed_elements {
			int_value := element.int()
			a[i][j] = fractions.fraction(int_value, 1)
		}
	}

	// we chop off the last one to deal with the fact the last line also has a trailing \\
	return a[..a.len-1]
}

// test_string := r"1&2&3\\4&-5&6\\7&8.0010&9"

// println(parse_matrix(test_string))