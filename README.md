# RREF to LaTeX

A program that uses Gauss-Jordan Elimination to find the RREF of a matrix, automatically outputting the steps in LaTeX format along the way

## Description
Given a matrix, this program step-by-step finds the RREF and outputs the matrix after each step in a format that can be easily copypasted into a program like [Obsidian](https://obsidian.md/) or your favorite LaTeX editor

## Installation
### Install V
This program runs on [The V Programming Language](https://vlang.io/) so you'll need to have that installed. Follow the instructions [here](https://github.com/vlang/v#installing-v-from-source).

### Download this program
`git clone https://gitlab.com/1ethanhansen/rref-to-latex.git`

## Usage
Move into the directory and run `v run . [output_mode]`

`output_mode` is either `obsidian` (the default) or `latex`

It will ask for the number of rows and number of columns. Then you enter the rows using ampersands (`&`) to separate the elements. Floats are allowed, but fractions don't work

You can copypaste the output into obsidian/latex editor, just watch out for sometimes it will output a -0 which doesn't really make sense

## Support
Open an issue here and I'll see it

## Roadmap
Check out todo.md

## Contributing
Open a Merge Request and if it's good I'll allow it

## Project status
I'm kinda done, it does what I need it to
