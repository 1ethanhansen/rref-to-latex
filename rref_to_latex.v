import os
import math.fractions
import math { round, close }
import matrix_parser

fn matrix_to_latex(matrix [][]fractions.Fraction, mode string) {

	mut front_endcap := "$$"
	mut back_endcap := "$$"
	begin := r"\begin{bmatrix} "
	end := r" \end{bmatrix}"

	if mode == "obsidian" {
		
	} else if mode == "latex" {
		front_endcap = "\["
		back_endcap = "\]"
	}
	mut full_string := front_endcap + " " + begin
	for row in matrix {
		for element in row {
			if int(element.f64()) == element.f64() {
				full_string += int(element.f64()).str() + " & "
			} else {
				full_string += element.str() + " & "
			}
		}
		full_string = full_string[0..full_string.len-2]
		full_string += r"\\"
	}
	full_string += end + " " + back_endcap
	println(full_string)
}

fn matrix_to_rref(matrix [][]fractions.Fraction, mode string) [][]fractions.Fraction {
	// used to mxn matrices so pull those values out
	m := matrix.len
	n := matrix[0].len

	mut new_matrix := matrix.clone()
	// for tracking where our last pivot value was
	mut prev_pivot_row := -1
	// try to get pivots in every column
	for column in 0..n {
		// if we've run out of rows, just stop
		if prev_pivot_row + 1 == m {
			break
		}
		// look through the lower rows for pivots
		for row in (prev_pivot_row+1)..m {
			// pivots have to start off non-zero
			if !new_matrix[row][column].equals(fractions.fraction(0, 1)) {
				// we found a pivot-in-the-rough!
				pivot := new_matrix[row][column]
				// store the row we found it in
				mut current_pivot_row := row
				// if it wasn't in the next row, swap the current row and prev_pivot_row+1
				if row != prev_pivot_row + 1 {
					current_pivot_row = prev_pivot_row + 1
					for index in 0..n {
						temp := new_matrix[row][index]
						new_matrix[row][index] = new_matrix[current_pivot_row][index]
						new_matrix[current_pivot_row][index] = temp
					}
					matrix_to_latex(new_matrix, mode)
				}
				// divide everything in that row by the value, but only if the pivot wasn't 1
				if !pivot.equals(fractions.fraction(1, 1)) {
					for index in 0..n {
						mut new_val := new_matrix[current_pivot_row][index] / pivot
						new_matrix[current_pivot_row][index] = new_val
					}
					matrix_to_latex(new_matrix, mode)
				}
				// clear everything above and below the pivot
				for other_row in 0..m {
					if other_row == current_pivot_row {
						continue
					}
					rows_factor := fractions.fraction(-1, 1) * new_matrix[other_row][column]
					for other_column in 0..n {
						mut new_value := new_matrix[other_row][other_column] + rows_factor * new_matrix[current_pivot_row][other_column]
						new_matrix[other_row][other_column] = new_value
					}
					matrix_to_latex(new_matrix, mode)
				}
				// store our previous row for future reference
				prev_pivot_row = current_pivot_row
				// stop looking for pivots in this column
				break
			}
		}
	}

	return new_matrix
}

fn main() {
	// Get obsidian vs latex mode
	output_mode := os.args[1] or { "obsidian" }

	rows := os.input("Enter the number of rows: ").int()
	cols := os.input("Enter the number of columns: ").int()
	println("Finding RREF of a $rows x $cols matrix")

	mut matrix_string := ""

	// cli version
	for row in 0 .. rows {
		math_row := row + 1
		row_elems := os.input("Please enter row $math_row: ")
		matrix_string += row_elems + r"\\"
	}
	matrix := matrix_parser.parse_cli_matrix(matrix_string)

	matrix_to_latex(matrix, output_mode)

	new_matrix := matrix_to_rref(matrix, output_mode)
}